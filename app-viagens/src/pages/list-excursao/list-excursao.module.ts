import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListExcursaoPage } from './list-excursao';

@NgModule({
  declarations: [
    ListExcursaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListExcursaoPage),
  ],
})
export class ListExcursaoPageModule {}
