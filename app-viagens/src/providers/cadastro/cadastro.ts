import { IOperationsService } from './../util/IOperationsService';
import { UtilProvider } from './../util/util';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the CadastroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CadastroProvider {

  resource: string = "cadastro-usuario";

  constructor(public http: Http, private utilProvider: UtilProvider) {
    this.resource = this.utilProvider.url(this.resource);
  }

  getAll(): Observable<any[]> {
    return this.http.get(this.resource, this.utilProvider.getHeader())
    .map(this.utilProvider.getDatas)
    .catch(this.utilProvider.processError);

  }

  getById(id) : Observable<any>{
    return null;
  }

  save(entity: any): Observable<any>{
    return this.http.post(this.resource, entity, this.utilProvider.getHeader())
    .map(this.utilProvider.getDatas)
    .catch(this.utilProvider.processError);
  }

  update(id: any, entity: any): Observable<any>{
    return this.http.put(this.resource + '/' + id, entity, this.utilProvider.getHeader())
    .map(this.utilProvider.getDatas)
    .catch(this.utilProvider.processError);
  }

  delete(id: any): Observable<any>{
    return this.http.delete(this.resource + "/" + id, this.utilProvider.getHeader())
    .map(this.utilProvider.getDatas)
    .catch(this.utilProvider.processError);
  }

}
