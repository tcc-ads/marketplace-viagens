import { Observable } from 'rxjs/Observable';
import { PacoteProvider } from './../../providers/pacote/pacote';
import { FormPacotePage } from './../form-pacote/form-pacote';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the ListPacotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-pacote',
  templateUrl: 'list-pacote.html',
})
export class ListPacotePage {

  private pacotes: Observable<any[]>;

  constructor(
    private pacoteProvider: PacoteProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertContoller: AlertController) {
    this.getAll();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListExcursaoPage');
  }

  ionViewWillEnter() {
    this.getAll();
  }
  newPacote() {
    this.navCtrl.push(FormPacotePage);
  }

  getAll() {
    this.pacoteProvider.getAll().subscribe(data => {
      this.pacotes = data['pacotes'];
    },
      error => console.log('error'));
  }

  getById(id) {

  }

  remove(id) {
    this.pacoteProvider.delete(id).subscribe(data => this.ionViewWillEnter());
  }

  edit(id) {

  }

  itemTapped(event, item) {
    console.log(event);
    this.navCtrl.push(FormPacotePage, { pacote: item });
  }

  presentConfirm(id) {
    console.log(id);
    const alert = this.alertContoller.create({
      title: 'Excluir pacote',
      message: 'Deseja excluir esse pacote?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.remove(id);
          }
        }
      ]
    });
    alert.present();
  }
}
