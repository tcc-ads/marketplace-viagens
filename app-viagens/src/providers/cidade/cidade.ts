import { UtilProvider } from './../util/util';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the CidadeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CidadeProvider {

  resource: string = "cidades";

  constructor(public http: Http, private utilProvider: UtilProvider) {
    this.resource = this.utilProvider.url(this.resource);
  }

  getAll() : Observable<any[]> {

    return this.http.get(this.resource, this.utilProvider.getHeader())
      .map(this.utilProvider.getDatas)
      .catch(this.utilProvider.processError);
  }
}
