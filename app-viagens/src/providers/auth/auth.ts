import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  private token: string;
  private user: any = {};

  private API_URL: string = 'http://viagens.jeffersoncruz.com/api/v1/login';

  constructor(public http: Http) {
    console.log('Hello AuthProvider Provider');
  }

  getUser() {
    return this.user;
  }

  logIn(user: string, password: string): Observable<any> {
    //Requisição para api, passando usuário e senha e a api retorna o token para aplicação
    // Armazenar o token na store para ser usado em toda requisição
    let header = new Headers({ 'Content-Type': 'application/json' });

    return this.http.post(this.API_URL, header)
      .map(res => {
        user['name'] = "";
        user['id'] = "";

        localStorage.setItem('currentUser', JSON.stringify({currentUser: this.user, token: res['token']}));

        return true;
      })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  logOut() {
    //Remover o token da store
  }

  getToken() {
    return this.token;
  }

}
