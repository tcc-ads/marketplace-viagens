//import { AuthProvider } from './../auth/auth';
import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UtilProvider {

  private API_URL: string = 'http://viagens.jeffersoncruz.com/api/v1/';
  private user: any = {};
  //private authProvider: AuthProvider
  constructor() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  url(path: string) {
    return this.API_URL + path;
  }

  getHeader() {//this.user.token
    let headersParams = { 'Content-Type': 'application/json', 'Authorization': ''};
    
    let headers = new Headers(headersParams);
    let options = new RequestOptions({ headers: headers });
    
    return options;
  }

  getDatas(response: Response) {
    let data = response.json();
    return data || {};
  }

  processError(error: any) {
    return Observable.throw('Erro acessando servidor remoto.');
  }
}