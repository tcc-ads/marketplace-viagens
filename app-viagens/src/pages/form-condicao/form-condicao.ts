import { CondicaoProvider } from './../../providers/condicao/condicao';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the FormCondicaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-condicao',
  templateUrl: 'form-condicao.html',
})
export class FormCondicaoPage {

  private insert: boolean = false;

  form: FormGroup;
  //private excursao: any = {};
  private condicao: any = {
    condicao: '',
    usuario_id: 1
  };

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private condicaoProvider: CondicaoProvider,
    private alertController: AlertController) {
    this.initalize();

    if (this.navParams.get('condicao') != undefined) {
      this.condicao = this.navParams.get('condicao');
      this.fillFields(this.condicao);
      this.insert = false;
    } else
      this.insert = true;
  }

  fillFields(condicao) {
    this.form.controls['id'].setValue(condicao.id);
    this.form.controls['condicao'].setValue(condicao.condicao);
  }

  initalize() {
    // Todos os campos criados no formulario deveram ser referenciados aqui, sendo que o nome da propriedade
    // do objeto deverá ser o mesmo que estiver na proriedade formControlName no input html]

    this.form = this.formBuilder.group({
      id: [''],
      condicao: ['', Validators.required],
      usuario_id: ['']
    });

  }

  save() {

    console.log(this.insert);
    if (this.insert) {
      this.condicaoProvider.save(this.form.value).subscribe(data => {
        this.success();
      },
        error => console.log(error));
    } else {
       this.condicaoProvider.update(this.form.controls['id'].value, this.form.value).subscribe(data => {
        console.log(this.form.value);
        this.success();
      },
        error => console.log(error));
    }
  }

  updateRegister() {

  }

  newRegister() {

  }

  success() {
    const alert = this.alertController.create({
      title: 'Sucesso',
      message: 'Sua condição foi ' + (this.insert ? 'cadastrada' : 'atualizada'),
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

}
