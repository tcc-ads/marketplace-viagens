<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSaidasTable extends Migration {

	public function up()
	{
		Schema::create('saidas', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('google_places_id');
			$table->datetime('horario')->nullable();
			$table->integer('cidade_id')->unsigned();
			$table->integer('excursao_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('saidas');
	}
}