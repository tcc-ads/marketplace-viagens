<?php

namespace App\Http\Controllers;

use App\Destino;
use Illuminate\Http\Request;

class DestinoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $destinos = Destino::all();

        if (!$destinos) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhum destino foi encontrado',
                'destinos' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'Os destinos foram listados com sucesso',
            'destinos' => $destinos
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO Validação

        $destino = new Destino();
        $destino->fill($request->all());

        if (!$destino->save()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O destino não pôde ser criado',
            ], 500);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O destino foi criado com sucesso',
            'destino' => $destino,
            'show_destino' => url()->route('destinos.show', ['destino' => $destino])
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Destino $destino
     */
    public function show($id)
    {
        $destino = Destino::find($id);

        if (!$destino) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O destino não foi encontrado',
                'destino' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O destino foi encontrado com sucesso',
            'destino' => $destino
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Destino $destino
     */
    public function update(Request $request, $id)
    {
        $destino = Destino::find($id);

        if (!$destino) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O destino não foi encontrado',
                'destino' => null
            ], 404);
        }

        // TODO Validação

        if (!$destino->update($request->all())) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O destino não pôde ser atualizado',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O destino foi atualizado',
            'destino' => $destino,
            'show_destino' => url()->route('destinos.show', ['destino' => $destino])
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Destino $destino
     */
    public function destroy($id)
    {
        $destino = Destino::find($id);

        if (!$destino) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O destino não pôde encontrado',
                'destino' => null
            ], 404);
        }

        if (!$destino->delete()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O destino não pôde ser removido',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O destino foi removido',
        ], 200);
    }
}
