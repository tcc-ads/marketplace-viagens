import { Component } from '@angular/core';
import { CadastroProvider } from './../../providers/cadastro/cadastro';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the CadastroUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastro-usuario',
  templateUrl: 'cadastro-usuario.html',
})
export class CadastroUsuarioPage {

  private insert: boolean = false;

  form: FormGroup;

  cadastroUsuario: any = {};

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public navParams: NavParams,
    private cadastroProvider: CadastroProvider,
    private alertController: AlertController) {
    this.initalize();

    if (this.navParams.get('cadastroUsuario') != undefined) {
      this.cadastroUsuario = this.navParams.get('cadastroUsuario');
      this.insert = false;
    } else {
      this.insert = true;
    }
  }

  initalize() {
    this.form = this.formBuilder.group({
      nome: ['', Validators.required],
      email: ['', Validators.required],
      telefone: ['', Validators],
      senha: ['', Validators.required],
      confirmar: ['', Validators.required]
    });
  }

  save() {
    console.log(this.insert);
    if (this.insert) {
      this.cadastroProvider.save(this.form.value).subscribe(data => {
        this.success();
      },
        error => console.log(error));
    }
  }

  success(){
    const alert = this.alertController.create({
      title: 'Sucesso',
      message: 'Seu cadastro foi '+(this.insert? 'realizado' : 'atualizado'),
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

}
