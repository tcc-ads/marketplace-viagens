<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadosTable extends Migration {

	public function up()
	{
		Schema::create('estados', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome');
			$table->string('sigla', 2)->unique();
		});
	}

	public function down()
	{
		Schema::drop('estados');
	}
}