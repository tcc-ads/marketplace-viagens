<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $table = 'cidades';
    public $timestamps = false;
    protected $fillable = ['nome', 'estado_id'];

    public function estado()
    {
        return $this->belongsTo('App\Estado');
    }

    public function saidas()
    {
        return $this->hasMany('App\Saida');
    }

    public function usuarios()
    {
        return $this->hasMany('App\Usuario');
    }

    public function destinos()
    {
        return $this->hasMany('App\Destino');
    }

}