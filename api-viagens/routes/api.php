<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::get('/', function () {
        return response()->json(['message' => 'API Viagens', 'status' => 'Conectado']);
    })->name('api.root');

    Route::resource('categorias', 'CategoriaController', ['only' => ['index', 'show']]);
    Route::resource('destinos', 'DestinoController', ['except' => ['create', 'edit']]);
    Route::resource('excursoes', 'ExcursaoController', ['except' => ['create', 'edit']]);
    Route::resource('saidas', 'SaidaController', ['except' => ['create', 'edit']]);
    Route::resource('estados', 'EstadoController', ['only' => ['index', 'show']]);
    Route::resource('cidades', 'CidadeController', ['only' => ['index', 'show']]);
    Route::resource('usuarios', 'UsuarioController', ['except' => ['create', 'edit']]);
    Route::resource('pacotes', 'PacoteController', ['except' => ['create', 'edit']]);
    Route::resource('condicoes', 'CondicaoController', ['except' => ['create', 'edit']]);
    Route::resource('reservas', 'ReservaController', ['except' => ['create', 'edit']]);
});