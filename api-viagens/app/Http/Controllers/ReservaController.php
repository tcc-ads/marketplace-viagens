<?php

namespace App\Http\Controllers;

use App\Reserva;
use Illuminate\Http\Request;

class ReservaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservas = Reserva::all();

        if (!$reservas) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhuma reserva foi encontrada',
                'reservas' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'As reservas foram listadas com sucesso',
            'reservas' => $reservas
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO Validação

        $reserva = new Reserva();
        $reserva->fill($request->all());

        if (!$reserva->save()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A reserva não pôde ser criada',
            ], 500);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A reserva foi criada com sucesso',
            'reserva' => $reserva,
            'show_reserva' => url()->route('reservas.show', ['reserva' => $reserva])
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Reserva $reserva
     */
    public function show($id)
    {
        $reserva = Reserva::find($id);

        if (!$reserva) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A reserva não foi encontrada',
                'reserva' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A reserva foi encontrada com sucesso',
            'reserva' => $reserva
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Reserva $reserva
     */
    public function update(Request $request, $id)
    {
        $reserva = Reserva::find($id);

        if (!$reserva) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A reserva não foi encontrada',
                'reserva' => null
            ], 404);
        }

        // TODO Validação

        if (!$reserva->update($request->all())) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A reserva não pôde ser atualizada',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A reserva foi atualizada',
            'reserva' => $reserva,
            'show_reserva' => url()->route('reservas.show', ['reserva' => $reserva])
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Reserva $reserva
     */
    public function destroy($id)
    {
        $reserva = Reserva::find($id);

        if (!$reserva) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A reserva não pôde ser encontrada',
                'reserva' => null
            ], 404);
        }

        if (!$reserva->delete()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A reserva não pôde ser removida',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A reserva foi removida',
        ], 200);
    }
}
