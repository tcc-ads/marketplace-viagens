<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{

    protected $table = 'categorias';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['nome'];
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public function excursoes()
    {
        return $this->hasMany('App\Excursao');
    }

}