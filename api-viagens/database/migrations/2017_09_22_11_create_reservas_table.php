<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservasTable extends Migration {

	public function up()
	{
		Schema::create('reservas', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->text('comentarios')->nullable();
			$table->boolean('efetivada')->default(false);
			$table->integer('pacote_id')->unsigned()->nullable();
			$table->integer('condicao_id')->unsigned()->nullable();
			$table->integer('saida_id')->unsigned()->nullable();
			$table->integer('usuario_id')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::drop('reservas');
	}
}