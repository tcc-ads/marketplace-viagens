import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormDestinoPage } from './form-destino';

@NgModule({
  declarations: [
    FormDestinoPage,
  ],
  imports: [
    IonicPageModule.forChild(FormDestinoPage),
  ],
})
export class FormDestinoPageModule {}
