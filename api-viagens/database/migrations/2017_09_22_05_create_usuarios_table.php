<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariosTable extends Migration {

	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome');
			$table->string('email')->unique()->nullable();
			$table->string('senha')->nullable();
			$table->date('dt_nascimento')->nullable();
			$table->string('cpf')->unique()->nullable();
			$table->string('rg')->nullable();
			$table->string('endereco')->nullable();
			$table->string('telefone')->nullable();
			$table->string('celular')->nullable();
			$table->integer('cidade_id')->unsigned()->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('usuarios');
	}
}