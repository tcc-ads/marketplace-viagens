import { Observable } from 'rxjs/Observable';
import { FormDestinoPage } from './../form-destino/form-destino';
import { DestinoProvider } from './../../providers/destino/destino';
import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';

/**
 * Generated class for the ListDestinoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-destino',
  templateUrl: 'list-destino.html'
})
export class ListDestinoPage {

  private destinos: Observable<any[]>;

  constructor(
    private destinoProvider: DestinoProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertContoller: AlertController) {
    this.getAll();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListExcursaoPage');
  }

  ionViewWillEnter() {
    this.getAll();
  }
  newDestino() {
    this.navCtrl.push(FormDestinoPage);
  }

  getAll() {
    this.destinoProvider.getAll().subscribe(data => {
      this.destinos = data['destinos'];
    },
      error => console.log('error'));
  }

  getById(id) {

  }

  remove(id) {
    this.destinoProvider.delete(id).subscribe(data => this.ionViewWillEnter());
  }

  edit(id) {

  }

  itemTapped(event, item) {
    console.log(event);
    this.navCtrl.push(FormDestinoPage, { pacote: item });
  }

  presentConfirm(id) {
    console.log(id);
    const alert = this.alertContoller.create({
      title: 'Excluir destino',
      message: 'Deseja excluir esse destino?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.remove(id);
          }
        }
      ]
    });
    alert.present();
  }

}
