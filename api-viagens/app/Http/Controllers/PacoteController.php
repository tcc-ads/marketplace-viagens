<?php

namespace App\Http\Controllers;

use App\Pacote;
use Illuminate\Http\Request;

class PacoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pacotes = Pacote::all();

        if (!$pacotes) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhum pacote foi encontrado',
                'pacotes' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'Os pacotes foram listados com sucesso',
            'pacotes' => $pacotes
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO Validação

        $pacote = new Pacote();
        $pacote->fill($request->all());

        if (!$pacote->save()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O pacote não pôde ser criado',
            ], 500);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O pacote foi criado com sucesso',
            'pacote' => $pacote,
            'show_pacote' => url()->route('pacotes.show', ['pacote' => $pacote])
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Pacote $pacote
     */
    public function show($id)
    {
        $pacote = Pacote::find($id);

        if (!$pacote) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O pacote não foi encontrado',
                'pacote' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O pacote foi encontrado com sucesso',
            'pacote' => $pacote
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Pacote $pacote
     */
    public function update(Request $request, $id)
    {
        $pacote = Pacote::find($id);

        if (!$pacote) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O pacote não foi encontrado',
                'pacote' => null
            ], 404);
        }

        // TODO Validação

        if (!$pacote->update($request->all())) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O pacote não pôde ser atualizado',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O pacote foi atualizado',
            'pacote' => $pacote,
            'show_pacote' => url()->route('pacotes.show', ['pacote' => $pacote])
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Pacote $pacote
     */
    public function destroy($id)
    {
        $pacote = Pacote::find($id);

        if (!$pacote) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O pacote não pôde encontrado',
                'pacote' => null
            ], 404);
        }

        if (!$pacote->delete()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O pacote não pôde ser removido',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O pacote foi removido',
        ], 200);
    }
}
