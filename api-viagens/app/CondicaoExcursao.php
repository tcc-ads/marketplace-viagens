<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CondicaoExcursao extends Model
{

    protected $table = 'condicao_excursao';
    public $timestamps = true;
    protected $fillable = ['condicao_id', 'excursao_id'];

}