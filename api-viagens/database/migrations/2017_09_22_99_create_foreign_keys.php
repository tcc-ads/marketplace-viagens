<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('destinos', function(Blueprint $table) {
			$table->foreign('cidade_id')->references('id')->on('cidades')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('excursoes', function(Blueprint $table) {
			$table->foreign('categoria_id')->references('id')->on('categorias')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('excursoes', function(Blueprint $table) {
			$table->foreign('destino_id')->references('id')->on('destinos')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('excursoes', function(Blueprint $table) {
			$table->foreign('usuario_id')->references('id')->on('usuarios')
						->onDelete('set null')
						->onUpdate('cascade');
		});
		Schema::table('saidas', function(Blueprint $table) {
			$table->foreign('cidade_id')->references('id')->on('cidades')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('saidas', function(Blueprint $table) {
			$table->foreign('excursao_id')->references('id')->on('excursoes')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('cidades', function(Blueprint $table) {
			$table->foreign('estado_id')->references('id')->on('estados')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('usuarios', function(Blueprint $table) {
			$table->foreign('cidade_id')->references('id')->on('cidades')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('pacotes', function(Blueprint $table) {
			$table->foreign('excursao_id')->references('id')->on('excursoes')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('condicoes', function(Blueprint $table) {
			$table->foreign('usuario_id')->references('id')->on('usuarios')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('condicao_excursao', function(Blueprint $table) {
			$table->foreign('condicao_id')->references('id')->on('condicoes')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('condicao_excursao', function(Blueprint $table) {
			$table->foreign('excursao_id')->references('id')->on('excursoes')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->foreign('pacote_id')->references('id')->on('pacotes')
						->onDelete('set null')
						->onUpdate('cascade');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->foreign('condicao_id')->references('id')->on('condicoes')
						->onDelete('set null')
						->onUpdate('cascade');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->foreign('saida_id')->references('id')->on('saidas')
						->onDelete('set null')
						->onUpdate('cascade');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->foreign('usuario_id')->references('id')->on('usuarios')
						->onDelete('set null')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('destinos', function(Blueprint $table) {
			$table->dropForeign('destinos_cidade_id_foreign');
		});
		Schema::table('excursoes', function(Blueprint $table) {
			$table->dropForeign('excursoes_categoria_id_foreign');
		});
		Schema::table('excursoes', function(Blueprint $table) {
			$table->dropForeign('excursoes_destino_id_foreign');
		});
		Schema::table('excursoes', function(Blueprint $table) {
			$table->dropForeign('excursoes_usuario_id_foreign');
		});
		Schema::table('saidas', function(Blueprint $table) {
			$table->dropForeign('saidas_cidade_id_foreign');
		});
		Schema::table('saidas', function(Blueprint $table) {
			$table->dropForeign('saidas_excursao_id_foreign');
		});
		Schema::table('cidades', function(Blueprint $table) {
			$table->dropForeign('cidades_estado_id_foreign');
		});
		Schema::table('usuarios', function(Blueprint $table) {
			$table->dropForeign('usuarios_cidade_id_foreign');
		});
		Schema::table('pacotes', function(Blueprint $table) {
			$table->dropForeign('pacotes_excursao_id_foreign');
		});
		Schema::table('condicoes', function(Blueprint $table) {
			$table->dropForeign('condicoes_usuario_id_foreign');
		});
		Schema::table('condicao_excursao', function(Blueprint $table) {
			$table->dropForeign('condicao_excursao_condicao_id_foreign');
		});
		Schema::table('condicao_excursao', function(Blueprint $table) {
			$table->dropForeign('condicao_excursao_excursao_id_foreign');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->dropForeign('reservas_pacote_id_foreign');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->dropForeign('reservas_condicao_id_foreign');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->dropForeign('reservas_saida_id_foreign');
		});
		Schema::table('reservas', function(Blueprint $table) {
			$table->dropForeign('reservas_usuario_id_foreign');
		});
	}
}