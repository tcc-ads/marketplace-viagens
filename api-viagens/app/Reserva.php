<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reserva extends Model 
{

    protected $table = 'reservas';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['comentarios', 'efetivada', 'pacote_id', 'condicao_id', 'saida_id', 'usuario_id'];

    public function pacote()
    {
        return $this->belongsTo('App\Pacote');
    }

    public function condicao_pagamento()
    {
        return $this->hasOne('App\Condicao', 'condicao_id');
    }

    public function saida()
    {
        return $this->hasOne('App\Saida');
    }

    public function participante()
    {
        return $this->hasOne('App\Usuario');
    }

}