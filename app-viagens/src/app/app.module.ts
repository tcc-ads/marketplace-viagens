import { ListDestinoPageModule } from './../pages/list-destino/list-destino.module';
import { FormDestinoPageModule } from './../pages/form-destino/form-destino.module';
import { FormCondicaoPageModule } from './../pages/form-condicao/form-condicao.module';
import { ListCondicaoPageModule } from './../pages/list-condicao/list-condicao.module';
import { FormPacotePageModule } from './../pages/form-pacote/form-pacote.module';
import { ListPacotePageModule } from './../pages/list-pacote/list-pacote.module';
import { HomePage } from './../pages/home/home';
import {LoginPageModule} from './../pages/login/login.module';
import { IntroPageModule } from './../pages/intro/intro.module';
import {CadastroUsuarioPageModule} from './../pages/cadastro-usuario/cadastro-usuario.module';

import { FormExcursaoPageModule } from './../pages/form-excursao/form-excursao.module';
import { ListExcursaoPageModule } from './../pages/list-excursao/list-excursao.module';

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { ListPage } from '../pages/list/list';

import { HttpModule } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ExcursaoProvider } from '../providers/excursao/excursao';
import { UtilProvider } from '../providers/util/util';
import { AuthProvider } from '../providers/auth/auth';
import { CategoriaProvider } from '../providers/categoria/categoria';
import { DestinoProvider } from '../providers/destino/destino';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { CondicaoProvider } from '../providers/condicao/condicao';
import { SaidaProvider } from '../providers/saida/saida';
import { PacoteProvider } from '../providers/pacote/pacote';
import { ReservaProvider } from '../providers/reserva/reserva';
import { ReactiveFormsModule } from "@angular/forms";
import { CidadeProvider } from '../providers/cidade/cidade';
import { CadastroProvider } from '../providers/cadastro/cadastro';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),

    ReactiveFormsModule,
    ListExcursaoPageModule,
    ListPacotePageModule,
    FormPacotePageModule,
    FormExcursaoPageModule,
    ListCondicaoPageModule,
    FormCondicaoPageModule,
    ListDestinoPageModule,
    FormDestinoPageModule,
    HttpModule,
    LoginPageModule,
    IntroPageModule,
    CadastroUsuarioPageModule
   ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ExcursaoProvider,
    UtilProvider,
    AuthProvider,
    CategoriaProvider,
    DestinoProvider,
    UsuarioProvider,
    CondicaoProvider,
    SaidaProvider,
    PacoteProvider,
    ReservaProvider,
    CidadeProvider,
    CadastroProvider
  ]
})
export class AppModule {}
