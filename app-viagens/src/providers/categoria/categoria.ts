import { IOperationsService } from './../util/IOperationsService';

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { UtilProvider } from './../util/util';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the CategoriaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoriaProvider implements IOperationsService {

  resource: string = "categorias";

  constructor(public http: Http, private utilProvider: UtilProvider) {
    this.resource = this.utilProvider.url(this.resource);
  }

  getAll() : Observable<any[]> {

    return this.http.get(this.resource, this.utilProvider.getHeader())
      .map(this.utilProvider.getDatas)
      .catch(this.utilProvider.processError);
  }

  getById(id) : Observable<any> {
    
    return null;
  }

  save(entity: any): Observable<any> {
    return null;
  }

  update(id: any, entity: any): Observable<any> {
    return null;
  }

  delete(id: any): Observable<any> {
    return null;
  }


}
