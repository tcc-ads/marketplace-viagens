<?php

namespace App\Http\Controllers;

use App\Cidade;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CidadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cidades = Cidade::all();

        if ($cidades->count() == 0) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhuma cidade encontrada',
                'cidades' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'Cidades listadas com sucesso',
            'cidades' => $cidades
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Cidade $cidade
     */
    public function show($id)
    {
        $cidade = Cidade::find($id);

        if(!$cidade) {
            return response()->json([
               'status' => 'erro',
               'mensagem' => 'Cidade não encontrada',
               'cidade' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'Cidade encontrada com sucesso',
            'cidade' => $cidade
        ], 200);
    }
}
