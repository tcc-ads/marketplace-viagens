<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pacote extends Model
{

    protected $table = 'pacotes';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['titulo', 'descricao', 'preco', 'esgotado', 'excursao_id'];

    public function excursao()
    {
        return $this->belongsTo('App\Excursao');
    }

    public function reservas()
    {
        return $this->hasMany('App\Reserva');
    }

}