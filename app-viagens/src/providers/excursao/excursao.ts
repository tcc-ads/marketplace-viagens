import { IOperationsService } from './../util/IOperationsService';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';

import { UtilProvider } from './../util/util';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ExcursaoProvider implements IOperationsService {

  resource: string = "excursoes";

  constructor(public http: Http, private utilProvider: UtilProvider) {
    this.resource = this.utilProvider.url(this.resource);
  }

  getAll() : Observable<any[]> {

    return this.http.get(this.resource, this.utilProvider.getHeader())
      .map(this.utilProvider.getDatas)
      .catch(this.utilProvider.processError);
  }

  getById(id) : Observable<any> {
    
    return null;
  }

  save(entity: any): Observable<any> {
    return this.http.post(this.resource, entity, this.utilProvider.getHeader())
    .map(this.utilProvider.getDatas)
    .catch(this.utilProvider.processError);
  }

  update(id: any, entity: any): Observable<any> {
    return this.http.put(this.resource + '/' + id, entity, this.utilProvider.getHeader())
    .map(this.utilProvider.getDatas)
    .catch(this.utilProvider.processError);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(this.resource + "/" + id, this.utilProvider.getHeader())
    .map(this.utilProvider.getDatas)
    .catch(this.utilProvider.processError);
  }

}
