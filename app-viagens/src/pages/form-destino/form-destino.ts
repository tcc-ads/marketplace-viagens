import { CidadeProvider } from './../../providers/cidade/cidade';
import { DestinoProvider } from './../../providers/destino/destino';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the FormDestinoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-destino',
  templateUrl: 'form-destino.html',
})
export class FormDestinoPage {

  private insert: boolean = false;
  private cidades: any[];
  form: FormGroup;
  //private excursao: any = {};
  private destino: any = {
    id: '',
    cidade_id: 0,
    google_places_id: ''
  };

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private destinoProvider: DestinoProvider,
    private alertController: AlertController,
    private cidadeProvider: CidadeProvider) {
    this.initalize();

    if (this.navParams.get('destino') != undefined) {
      this.destino = this.navParams.get('destino');
      //this.fillFields(this.destino);
      this.insert = false;
    } else
      this.insert = true;

    this.loadCidades();

  }

  loadCidades() {
    //Fazendo a requisição para a api
    this.cidadeProvider.getAll().subscribe(data => console.log(data),
      error => console.log(error));
  }

  initalize() {
    // Todos os campos criados no formulario deveram ser referenciados aqui, sendo que o nome da propriedade
    // do objeto deverá ser o mesmo que estiver na proriedade formControlName no input html]

    this.form = this.formBuilder.group({
      id: [''],
      google_places_id: ['', Validators.required],
      cidade_id: ['', Validators.required]
    });

  }

  save() {
    //console.log('teste');
    console.log(this.form.valid);
    if (this.insert) {
      this.destinoProvider.save(this.form.value).subscribe(data => {
        this.success();
      },
        error => console.log(error));
    } else {
      this.destinoProvider.update(this.form.controls['id'].value, this.form.value).subscribe(data => {
        this.success();
      },
        error => console.log(error));
    }
  }
  updateRegister() {

  }

  newRegister() {

  }

  success() {
    const alert = this.alertController.create({
      title: 'Sucesso',
      message: 'O destino foi ' + (this.insert ? 'cadastrada' : 'atualizada'),
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

}
