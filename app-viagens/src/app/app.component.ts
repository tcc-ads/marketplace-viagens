import { ListDestinoPage } from './../pages/list-destino/list-destino';
import { ListCondicaoPage } from './../pages/list-condicao/list-condicao';
import { ListPacotePage } from './../pages/list-pacote/list-pacote';
import { HomePage } from './../pages/home/home';
import {LoginPage } from './../pages/login/login';
import {CadastroUsuarioPage} from './../pages/cadastro-usuario/cadastro-usuario';

import { ListExcursaoPage } from './../pages/list-excursao/list-excursao';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IntroPage } from "../pages/intro/intro";

import { ListPage } from '../pages/list/list';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = IntroPage
  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      //{ title: 'Procurar Excursão', component: '' },
      { title: 'Meus Destinos', component: ListDestinoPage },
      { title: 'Minhas Excursões', component: ListExcursaoPage },
      { title: 'Meus Pacotes', component: ListPacotePage},
	    { title: 'Minhas Condições pgto.', component: ListCondicaoPage},
      { title: 'Dados Pessoais', component: HomePage },
      //{ title: 'Login', component: LoginPage },
      { title: 'Cadastar Usuário', component: CadastroUsuarioPage}    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    //this.nav.setRoot(page.component);
  }
}
