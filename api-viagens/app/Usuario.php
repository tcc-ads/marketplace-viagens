<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usuario extends Model
{

    protected $table = 'usuarios';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['nome', 'email', 'senha', 'dt_nascimento', 'cpf', 'rg', 'endereco', 'telefone', 'celular', 'cidade_id'];

    public function cidade()
    {
        return $this->belongsTo('App\Cidade');
    }

    public function excursoes_organizadas()
    {
        return $this->hasMany('App\Excursao');
    }

    public function condicoes_pagamento()
    {
        return $this->hasMany('App\Condicao');
    }

}