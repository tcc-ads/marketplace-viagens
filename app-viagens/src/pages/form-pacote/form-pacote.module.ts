import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormPacotePage } from './form-pacote';

@NgModule({
  declarations: [
    FormPacotePage,
  ],
  imports: [
    IonicPageModule.forChild(FormPacotePage),
  ],
})
export class FormPacotePageModule {}
