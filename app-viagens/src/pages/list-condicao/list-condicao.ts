import { FormCondicaoPage } from './../form-condicao/form-condicao';
import { CondicaoProvider } from './../../providers/condicao/condicao';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the ListCondicaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-condicao',
  templateUrl: 'list-condicao.html',
})
export class ListCondicaoPage {

 private condicoes: Observable<any[]>;

  constructor(private condicaoProvider: CondicaoProvider, public navCtrl: NavController, public navParams: NavParams,
    private alertContoller: AlertController) {
    this.getAll();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListExcursaoPage');
  }

  ionViewWillEnter() {
    this.getAll();
  }
  newCondicao() {
    this.navCtrl.push(FormCondicaoPage);
  }

  getAll() {
    this.condicaoProvider.getAll().subscribe(data => {
      this.condicoes = data['condicoes'];
    },
      error => console.log('error'));
  }

  getById(id) {

  }

  remove(id) {
    this.condicaoProvider.delete(id).subscribe(data => this.ionViewWillEnter());
  }

  edit(id) {

  }

  itemTapped(event, item) {
    console.log(event);
    this.navCtrl.push(FormCondicaoPage, { condicao: item });
  }

  presentConfirm(id) {
    console.log(id);
    const alert = this.alertContoller.create({
      title: 'Excluir excursão',
      message: 'Deseja excluir essa condição?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.remove(id);
          }
        }
      ]
    });
    alert.present();
  }

}
