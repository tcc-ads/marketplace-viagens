import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormExcursaoPage } from './form-excursao';

@NgModule({
  declarations: [
    FormExcursaoPage,
  ],
  imports: [
    IonicPageModule.forChild(FormExcursaoPage),
  ],
})
export class FormExcursaoPageModule {}
