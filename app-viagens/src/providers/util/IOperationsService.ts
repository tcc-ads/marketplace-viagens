import { Observable } from 'rxjs/Observable';

export interface IOperationsService {
    getAll() : Observable<any[]>;

    getById(id) : Observable<any>;

    save(entity: any) : Observable<any>;

    update(id: any, entity: any) : Observable<any>;

    delete(id: any) : Observable<any>;
}